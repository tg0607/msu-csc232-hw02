/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *  		Trevor Glauz <Glauz0607@live.missouristate.edu>
 *          
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

/**
 * Function prototype for a 2-variable ackermann function, 
 * which requires the parameters to be nonnegative
 */
unsigned int Acker(unsigned int a, unsigned int b);

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {
    std::cout << "Acker(1, 2) = " << Acker(1, 2) 
              << std::endl;
    return EXIT_SUCCESS;
}

/**
 * Definition of the 2-variable ackermann function
 */
unsigned int Acker(unsigned int a, unsigned int b) {
	unsigned int result; // value to be returned at the end of the function
	
	/**
	 * The ackermann function logic
	 */
	if (a == 0) // if a is equal to zero...
		result = b + 1;
	else if (b == 0) //assuming that a is greater than zero, if b is equal to zero...
		result = Acker(a - 1, 1);
	else //assuming that both a and b are greater than zero...
		result = Acker(a - 1, Acker(a, b - 1));
	
	return result;
}